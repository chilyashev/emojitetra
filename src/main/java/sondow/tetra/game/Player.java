package sondow.tetra.game;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import sondow.tetra.conf.TetraConfig;
import sondow.tetra.conf.TetraConfigFactory;
import sondow.tetra.conf.Time;
import sondow.tetra.conf.Tips;
import sondow.tetra.conf.TweetContent;
import sondow.tetra.io.Converter;
import sondow.tetra.io.Database;
import sondow.tetra.io.EmojiSet;
import sondow.tetra.io.Outcome;
import sondow.tetra.io.Referee;
import sondow.tetra.io.TwitterPollMaker;
import twitter4j.Card;
import twitter4j.Status;
import twitter4j.StatusWithCard;

import static java.time.temporal.ChronoUnit.SECONDS;

/**
 * Executes a game turn from start to finish.
 */
public class Player {

    private TetraConfig tetraConfig;
    private Database database;
    private Random random;
    private TwitterPollMaker pollMaker;
    private String twitterHandle;
    private Time time;

    public Player(Random random, Time time, Database database, TwitterPollMaker pollMaker) {
        init();
        this.random = random;
        this.time = time;
        this.database = database;
        this.pollMaker = pollMaker;
    }

    public Player() {
        init();
        this.random = new Random();
        this.time = new Time();
        this.database = new Database(tetraConfig, twitterHandle);
        this.pollMaker = new TwitterPollMaker(tetraConfig.getTwitterConfig());
    }

    private void init() {
        this.tetraConfig = new TetraConfigFactory().configure();
        this.twitterHandle = tetraConfig.getTwitterConfig().getUser();
    }

    public Outcome play() {
        String stateJson = database.readGameState();
        System.out.println("Read game state from database: " + stateJson);
        Converter converter = new Converter(random);
        Game game;
        if (stateJson == null) {
            EmojiSet emojiSet = EmojiSet.pickOne(random);
            game = new Game(emojiSet, random);
            game.spawnPiece();
        } else {
            game = converter.makeGameFromJson(stateJson);
        }
        Outcome outcome = readPreviousPollAndPostNewTweet(game, false);
        int maxRetries = 3;
        for (int i = 1; i <= maxRetries && outcome.getRetryDelaySeconds() >= 1; i++) {
            System.out.println("retry " + i + ", outcome.getRetryDelaySeconds(): " + outcome
                    .getRetryDelaySeconds());
            waitForPollEnd(outcome.getRetryDelaySeconds());
            // Sometimes Twitter takes way too long to process finishing a poll, and I don't want
            // to wait indefinitely for that opaque and unreliable process to finish.
            boolean giveUpAndDoItAnyway = (i >= maxRetries);
            outcome = readPreviousPollAndPostNewTweet(game, giveUpAndDoItAnyway);
        }
        Status tweet = outcome.getTweet();
        if (tweet != null) {
            game.setTweetId(tweet.getId());
            game.setThreadLength(game.getThreadLength() + 1);
            if (game.isGameOver()) {
                database.deleteGameState();
            } else {
                String jsonFromGame = converter.makeJsonFromGame(game);
                System.out.println("Writing game state to database: " + jsonFromGame);
                database.writeGameState(jsonFromGame);
            }
        }
        return outcome;
    }

    private Outcome readPreviousPollAndPostNewTweet(Game game, boolean giveUpAndDoItAnyway) {
        Outcome outcome = new Outcome();
        Integer turnLengthMinutes = tetraConfig.getTurnLengthMinutes();
        Long tweetId = game.getTweetId();
        StatusWithCard previous = pollMaker.readPreviousTweet(tweetId);
        if (previous == null || shouldAssessResultsNow(giveUpAndDoItAnyway, outcome, previous)) {
            Status tweet = postTweets(game, turnLengthMinutes, pollMaker, previous);
            outcome.setTweet(tweet);
        }
        return outcome;
    }

    private boolean shouldAssessResultsNow(boolean giveUpAndDoItAnyway, Outcome
            outcome, StatusWithCard previousTweet) {

        Card poll = previousTweet.getCard();
        boolean countsAreFinal = poll.isCountsAreFinal();
        System.out.println("are poll counts final? " + countsAreFinal);
        boolean assessResultsNow;
        if (giveUpAndDoItAnyway || countsAreFinal) {
            assessResultsNow = true;
        } else {
            Referee referee = new Referee();
            if (referee.hasSuperMajority(poll)) {
                assessResultsNow = true;
            } else {
                long tweetId = previousTweet.getId();
                long pollTimeSecondsRemaining = calculatePollSecondsRemaining(poll);
                if (pollTimeSecondsRemaining > 60) {
                    throw new RuntimeException("Close race ends much later. secondsRemaining=" +
                            pollTimeSecondsRemaining + " tweetId=" + tweetId);
                }
                outcome.setRetryDelaySeconds(pollTimeSecondsRemaining + 5);
                assessResultsNow = false;
            }
        }
        return assessResultsNow;
    }

    private Status postTweets(Game game, Integer turnLengthMinutes, TwitterPollMaker pollMaker,
                              StatusWithCard previousGameTweet) {
        Referee referee = new Referee();
        Long inReplyToStatusIdForNewGameTweet = null;
        boolean timeToBreakThread = (game.getThreadLength() >= 50);
        if (previousGameTweet != null) {
            final long previousGameTweetId = previousGameTweet.getId();
            inReplyToStatusIdForNewGameTweet = previousGameTweetId; // If continuing thread
            Move move = referee.determineElectedMove(previousGameTweet);
            if (move != null) {
                try {
                    game.doMove(move);
                } catch (GameOverException e) {
                    System.out.println("Game over with score " + game.getScore());
                }
            }

            if (timeToBreakThread) {
                // Start a new thread.

                // Three new tweets.

                // 1. newThreadIntroTweet: In reply to null, the start of the new thread should
                // be a tweet, possibly with a short message explaining that we're continuing a
                // game from another thread, and a link back to the last game tweet of the old
                // thread.
                // 2. newGameTweet: In reply to tweet 1, newThreadIntroTweet, make the
                // newGameTweet.
                // 3. oldThreadOutroTweet: In reply to the last game tweet of the old thread, a
                // tweet containing a link to tweet 2, newGameTweet.
                String previousGameTweetUrl = buildTweetUrl(previousGameTweetId);
                String newThreadIntroText = "Continuing game from thread… " + previousGameTweetUrl;
                Status newThreadIntroTweet = pollMaker.tweet(newThreadIntroText, null, null);
                inReplyToStatusIdForNewGameTweet = newThreadIntroTweet.getId();

            }
        }

        String text = game.toString();
        Status newGameTweet;
        if (game.isGameOver()) {
            // Game over tweet has no poll
            newGameTweet = pollMaker.tweet(text, inReplyToStatusIdForNewGameTweet, null);
        } else {
            List<String> choices = Move.toPollChoices(game.listLegalMoves());
            newGameTweet = pollMaker.postPoll(turnLengthMinutes, text, choices,
                    inReplyToStatusIdForNewGameTweet);
        }

        if (previousGameTweet != null && timeToBreakThread) {

            String newGameTweetUrl = buildTweetUrl(newGameTweet.getId());

            pollMaker.tweet("Game continues in new thread… " + newGameTweetUrl, previousGameTweet
                    .getId(), null);

            game.setThreadLength(0);
        }

        Tips tips = new Tips();
        ZonedDateTime now = time.nowZonedDateTime();
        if (tips.isTimeForTipMessage(now, tetraConfig.getTurnLengthMinutes())) {
            time.waitASec();
            TweetContent content = tips.getMessageForDayOfMonth(now.getDayOfMonth());
            pollMaker.tweet(content.getMessage(), null, content.getFile());
        }

        return newGameTweet;
    }

    private String buildTweetUrl(long id) {
        return "https://twitter.com/" + twitterHandle + "/status/" + id;
    }

    private long calculatePollSecondsRemaining(Card poll) {
        Instant endDatetimeUtc = poll.getEndDatetimeUtc();
        Instant now = Instant.now();
        long pollTimeSecondsRemaining = Math.max(0, now.until(endDatetimeUtc, SECONDS));
        System.out.println("now = " + now + ", endDatetimeUtc = " + endDatetimeUtc +
                ", pollTimeSecondsRemaining = " + pollTimeSecondsRemaining);
        return pollTimeSecondsRemaining;
    }

    private void waitForPollEnd(long secondsToWait) {
        try {
            System.out.println("Waiting " + secondsToWait + " seconds.");
            Thread.sleep(secondsToWait * 1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public static void main(String[] args) {
        Player player = new Player();
        player.play();
    }
}
