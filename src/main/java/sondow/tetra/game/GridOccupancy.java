package sondow.tetra.game;

public enum GridOccupancy {
    OCCUPIED, VACANT
}
