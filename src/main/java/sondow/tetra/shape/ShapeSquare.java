package sondow.tetra.shape;

import java.util.Arrays;
import java.util.List;
import sondow.tetra.game.BadCoordinatesException;

/**
 * <code>ShapeSquare</code> is a configuration of <code>Tile</code> objects arranged in a 2x2
 * square.
 * <p>
 * The configuration of this piece for each direction appears as follows. The rotation symbol in
 * each diagram
 * indicates the position of the hub square around which the piece rotates.
 * <p>
 * <code>
 * <p>
 * NORTH, WEST, SOUTH, EAST
 * 🔄🐯
 * 🐯🐯
 * <p>
 * </code>
 *
 * @author @JoeSondow
 */
public class ShapeSquare extends Shape {

    /**
     * Creates a new <code>ShapeSquare</code> object, also called a piece.
     *
     * @param config the Game object that owns the new piece, and Direction the piece faces
     */
    ShapeSquare(ShapeConfig config) {
        super(ShapeType.SQUARE, config);
    }

    @Override
    public ShapeType getShapeType() {
        return ShapeType.SQUARE;
    }

    /**
     * Updates the positions of all the tiles in this piece, based on: <br>
     * - the configuration of <code>ShapeSquare</code>, <br>
     * - the piece's current rotation (NORTH, EAST, SOUTH, or WEST), <br>
     * - the column index and row index of the hub square for this piece
     *
     * @throws BadCoordinatesException If updating the tile positions causes an illegal situation,
     *                                 such as a tile intersecting another or being off the game
     *                                 grid. This generally results in a "Game Over" condition.
     */
    @Override
    public void updateTiles() throws BadCoordinatesException {
        int col = columnIndex, row = rowIndex;

        // Configuration appears as follows (@ is hub square):
        //
        // @+
        // ++
        setTilePosition(1, col, row);
        setTilePosition(2, col + 1, row);
        setTilePosition(3, col + 1, row + 1);
        setTilePosition(4, col, row + 1);
    }

    /**
     * Rotating a square piece does nothing, so rotate() is overridden to skip all the usual
     * rotation code.
     */
    @Override
    public void rotate() {
    }

    static List<String> displayTall(String symbol) {
        return Arrays.asList(symbol + symbol, symbol + symbol);
    }
}
