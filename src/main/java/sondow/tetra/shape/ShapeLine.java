package sondow.tetra.shape;

import java.util.Arrays;
import java.util.List;
import sondow.tetra.game.BadCoordinatesException;
import sondow.tetra.game.Direction;

/**
 * <code>ShapeLine</code> is a configuration of <code>Tile</code> objects where the tiles are
 * always in a straight line.
 * <p>
 * Game play note: this is usually considered the most valuable piece because it can always fit
 * into deep crevices without leaving empty spaces below it.
 * <p>
 * The configuration of this piece for each direction appears as follows. The rotation symbol in
 * each diagram indicates the position of the hub square around which the piece rotates.
 * <p>
 * <code>
 * <p>
 * NORTH
 * 🐯🔄🐯🐯
 * <p>
 * WEST
 * 🐯
 * 🐯
 * 🔄
 * 🐯
 * <p>
 * SOUTH
 * 🐯🔄🐯🐯
 * <p>
 * EAST
 * 🐯
 * 🐯
 * 🔄
 * 🐯
 * <p>
 * </code>
 *
 * @author @JoeSondow
 */
public class ShapeLine extends Shape {

    /**
     * Creates a new <code>ShapeLine</code> object, also called a piece.
     *
     * @param config the Game object that owns the new piece, and Direction the piece faces
     */
    ShapeLine(ShapeConfig config) {
        super(ShapeType.LINE, config);
    }

    @Override
    public ShapeType getShapeType() {
        return ShapeType.LINE;
    }

    /**
     * Updates the positions of all the tiles in this piece, based on: <br>
     * - the configuration of <code>ShapeLine</code>, <br>
     * - the piece's current rotation (NORTH, EAST, SOUTH, or WEST), <br>
     * - the column index and row index of the hub square for this piece
     *
     * @throws BadCoordinatesException If updating the tile positions causes an illegal situation,
     *                                 such as a tile intersecting another or being off the game
     *                                 grid. This generally results in a "Game Over" condition.
     */
    @Override
    public void updateTiles() throws BadCoordinatesException {
        int col = columnIndex, row = rowIndex;
        if (direction == Direction.NORTH || direction == Direction.SOUTH) {
            // Configuration appears as follows (@ is hub square):
            //
            // +@++
            setTilePosition(1, col, row);
            setTilePosition(2, col - 1, row);
            setTilePosition(3, col + 1, row);
            setTilePosition(4, col + 2, row);
        } else if (direction == Direction.EAST || direction == Direction.WEST) {
            // Configuration appears as follows (@ is hub square):
            //
            // +
            // +
            // @
            // +
            setTilePosition(1, col, row);
            setTilePosition(2, col, row + 1);
            setTilePosition(3, col, row - 1);
            setTilePosition(4, col, row - 2);
        }
    }

    static List<String> displayTall(String symbol) {
        return Arrays.asList(symbol, symbol, symbol, symbol);
    }
}
