package sondow.tetra.conf;

import java.time.ZonedDateTime;

public class Time {

    public ZonedDateTime nowZonedDateTime() {
        return ZonedDateTime.now();
    }

    public void waitASec() {
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }
}
