package sondow.tetra.game;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sondow.tetra.io.Converter;
import sondow.tetra.io.EmojiSet;
import sondow.tetra.shape.Shape;
import sondow.tetra.shape.ShapeType;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static sondow.tetra.game.Direction.EAST;
import static sondow.tetra.game.Direction.NORTH;
import static sondow.tetra.game.Direction.SOUTH;
import static sondow.tetra.game.Move.DOWN;
import static sondow.tetra.game.Move.LEFT;
import static sondow.tetra.game.Move.LEFT_OR_RIGHT;
import static sondow.tetra.game.Move.PLUMMET;
import static sondow.tetra.game.Move.RIGHT;
import static sondow.tetra.game.Move.ROTATE;
import static sondow.tetra.game.Move.STOP;

@SuppressWarnings("UnnecessaryLocalVariable")
public class GameTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    private List<Move> list(Move... moves) {
        return Arrays.asList(moves);
    }

    private List<Move> LEFT_RIGHT_ROTATE_DOWN = list(LEFT, RIGHT, ROTATE, DOWN);
    private List<Move> LEFT_RIGHT_ROTATE_STOP = list(LEFT, RIGHT, ROTATE, STOP);
    private List<Move> LEFT_RIGHT_DOWN_PLUMMET = list(LEFT, RIGHT, DOWN, PLUMMET);
    private List<Move> LEFTORRIGHT_ROTATE_DOWN_PLUMMET = list(LEFT_OR_RIGHT, ROTATE, DOWN, PLUMMET);
    private List<Move> RIGHT_ROTATE_DOWN_PLUMMET = list(RIGHT, ROTATE, DOWN, PLUMMET);
    private List<Move> LEFT_ROTATE_DOWN_PLUMMET = list(LEFT, ROTATE, DOWN, PLUMMET);

    @Test
    public void shapeFrequenciesAreRoughlyEven() {

        Map<ShapeType, AtomicLong> shapeCounts = new HashMap<>();

        long total = 10000;
        int types = 7;
        long avg = (total / types);
        double min = avg * 0.88;
        double max = avg * 1.12;
        Set<ShapeType> shapeTypes = new TreeSet<>(Arrays.asList(ShapeType.values()));

        for (int i = 0; i < total; i++) {
            Game game = new Game(EmojiSet.AQUATIC, new Random(12));
            ShapeType shapeType = ShapeType.createAny(new Random(), game).getShapeType();
            AtomicLong count = shapeCounts.get(shapeType);
            if (count == null) {
                shapeCounts.put(shapeType, new AtomicLong(1L));
            } else {
                count.incrementAndGet();
            }
        }

        assertEquals(shapeTypes, new TreeSet<>(shapeCounts.keySet()));
        assertEquals(types, shapeCounts.keySet().size());
        for (ShapeType shapeType : shapeCounts.keySet()) {
            long count = shapeCounts.get(shapeType).get();
            String msg = "count=" + count + ", avg=" + avg + ", min=" + min + "," + " max=" + max;
            assertTrue(msg, count > min && count < max);
        }
    }

    @Test
    public void testCreateTetraGame() {
        Game game = new Game(EmojiSet.FRUIT, new Random(7));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.JAY, game, NORTH);
        game.setPiece(shape);
        game.setScore(9876543);
        String expected = "" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽🍏◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🍏🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　9876543\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(expected, game.toString());
    }

    @Test
    public void testGameOverScreen() {
        Game game = new Game(EmojiSet.FRUIT, new Random(7));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.JAY, game, NORTH);
        game.setPiece(shape);
        String expected = "" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽🍏◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🍏🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(expected, game.toString());
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.endGame();
        String gameOverExpected = "" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽🍏◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🍏🍏\n" +
                "⚡️⚡️⚡️⚡️⚡️⚡️⚡️　🍏\n" +
                "⚡️\uD83C\uDDEC \uD83C\uDDE6 \uD83C\uDDF2 \uD83C\uDDEA⚡️⚡️　🍏\n" +
                "⚡️⚡️\uD83C\uDDF4 \uD83C\uDDFB \uD83C\uDDEA \uD83C\uDDF7⚡️\n" +
                "⚡️⚡️⚡️⚡️⚡️⚡️⚡️\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(gameOverExpected, game.toString());
    }

    @Test
    public void testLeftOrRightCombinationChoice() {
        Game game = new Game(EmojiSet.CLOTHING, new Random(6));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.TEE, game, NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String startExpected = "" +
                "◽◽👔👔👔◽◽\n" +
                "◽◽◽👔◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(null, game.getPiece().getPreviousMove());
        game.doMove(DOWN);
        assertEquals(DOWN, game.getPiece().getPreviousMove());
        List<Move> legalMovesAfterDown = game.listLegalMoves();
        String afterOneDown = game.toString();
        String afterOneDownExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽👔👔👔◽◽\n" +
                "◽◽◽👔◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(LEFT_OR_RIGHT);
        assertEquals(LEFT_OR_RIGHT, game.getPiece().getPreviousMove());
        List<Move> legalMovesAfterLeftOrRightCombo = game.listLegalMoves();
        String afterLeftOrRightComboExpected = afterOneDownExpected;
        String afterLeftOrRightCombo = game.toString();

        assertEquals(startExpected, start);
        assertEquals(afterOneDownExpected, afterOneDown);
        assertEquals(LEFTORRIGHT_ROTATE_DOWN_PLUMMET, legalMovesAfterDown);
        assertEquals(afterLeftOrRightComboExpected, afterLeftOrRightCombo);
        assertEquals(Arrays.asList(LEFT, RIGHT), legalMovesAfterLeftOrRightCombo);
    }

    @Test
    public void testDown() {
        Game game = new Game(EmojiSet.CLOTHING, new Random(6));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.SQUARE, game, NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String startExpected = "" +
                "◽◽◽🎽🎽◽◽\n" +
                "◽◽◽🎽🎽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(DOWN);
        String afterOneDown = game.toString();
        String afterOneDownExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🎽🎽◽◽\n" +
                "◽◽◽🎽🎽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(DOWN);
        String afterTwoDownExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🎽🎽◽◽　Score\n" +
                "◽◽◽🎽🎽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        String afterTwoDown = game.toString();
        game.doMove(DOWN);
        String afterThreeDownExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽🎽🎽◽◽　0\n" +
                "◽◽◽🎽🎽◽◽\n" +
                "◽◽◽◽◽◽◽";
        String afterThreeDown = game.toString();
        game.doMove(DOWN);
        String afterFourDown = game.toString();
        String afterFourDownExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽⬛⬛◽◽\n" +
                "◽◽◽⬛⬛◽◽";

        assertEquals(startExpected, start);

        assertEquals(afterOneDownExpected, afterOneDown);
        assertEquals(afterTwoDownExpected, afterTwoDown);
        assertEquals(afterThreeDownExpected, afterThreeDown);
        assertEquals(afterFourDownExpected, afterFourDown);
    }

    @Test
    public void testPlummet() {
        Game game = new Game(EmojiSet.BOOK, new Random(28));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.ZEE, game, NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String startExpected = "" +
                "◽◽📕📕◽◽◽\n" +
                "◽◽◽📕📕◽◽　Next\n" +
                "◽◽◽◽◽◽◽　📚\n" +
                "◽◽◽◽◽◽◽　📚📚\n" +
                "◽◽◽◽◽◽◽　◽📚\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(PLUMMET);
        String afterOneDown = game.toString();
        String afterOneDownExpected = "" +
                "◽◽◽📚📚◽◽\n" +
                "◽◽📚📚◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　📙\n" +
                "◽◽◽◽◽◽◽　📙\n" +
                "◽◽◽◽◽◽◽　📙\n" +
                "◽◽◽◽◽◽◽　📙\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽📕📕◽◽◽\n" +
                "◽◽◽📕📕◽◽";
        game.doMove(DOWN);

        assertEquals(startExpected, start);
        assertEquals(afterOneDownExpected, afterOneDown);
    }

    @Test
    public void testLeft() {
        Game game = new Game(EmojiSet.BOOK, new Random(47));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.ESS, game, NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String startExpected = "" +
                "◽◽◽📚📚◽◽\n" +
                "◽◽📚📚◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　📚\n" +
                "◽◽◽◽◽◽◽　📚📚\n" +
                "◽◽◽◽◽◽◽　◽📚\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(LEFT);
        String afterOneLeft = game.toString();
        String afterOneLeftExpected = "" +
                "◽◽📚📚◽◽◽\n" +
                "◽📚📚◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　📚\n" +
                "◽◽◽◽◽◽◽　📚📚\n" +
                "◽◽◽◽◽◽◽　◽📚\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(LEFT);
        String afterTwoLeft = game.toString();
        String afterTwoLeftExpected = "" +
                "◽📚📚◽◽◽◽\n" +
                "📚📚◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　📚\n" +
                "◽◽◽◽◽◽◽　📚📚\n" +
                "◽◽◽◽◽◽◽　◽📚\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(Arrays.asList(RIGHT, ROTATE, DOWN, PLUMMET), game.listLegalMoves());
        assertEquals(startExpected, start);
        assertEquals(afterOneLeftExpected, afterOneLeft);
        assertEquals(afterTwoLeftExpected, afterTwoLeft);
    }

    @Test
    public void testRight() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(12));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.LINE, game, NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String startExpected = "" +
                "◽◽🐡🐡🐡🐡◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐌\n" +
                "◽◽◽◽◽◽◽　🐌🐌\n" +
                "◽◽◽◽◽◽◽　◽🐌\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(RIGHT);
        String afterOneRight = game.toString();
        String afterOneRightExpected = "" +
                "◽◽◽🐡🐡🐡🐡\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐌\n" +
                "◽◽◽◽◽◽◽　🐌🐌\n" +
                "◽◽◽◽◽◽◽　◽🐌\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(Arrays.asList(LEFT, ROTATE, DOWN, PLUMMET), game.listLegalMoves());
        assertEquals(startExpected, start);
        assertEquals(afterOneRightExpected, afterOneRight);
    }

    @Test
    public void testRotateEssQuarce() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.ESS, game, NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽🐼🐼◽◽\n" +
                "◽◽🐼🐼◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽　🐵🐵\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽　🐵🐵\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽🐼🐼◽◽\n" +
                "◽◽🐼🐼◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFTORRIGHT_ROTATE_DOWN_PLUMMET, game.listLegalMoves());
        game.doMove(ROTATE);
        String afterDownOneRotateOne = game.toString();
        String afterDownOneRotateOneExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽　🐵🐵\n" +
                "◽◽◽🐼◽◽◽　🐵\n" +
                "◽◽◽🐼🐼◽◽\n" +
                "◽◽◽◽🐼◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(ROTATE);
        String afterDownOneRotateTwo = game.toString();
        String afterDownOneRotateTwoExpected = afterDownExpected;
        game.doMove(ROTATE);
        String afterDownOneRotateThree = game.toString();
        String afterDownOneRotateThreeExpected = afterDownOneRotateOneExpected;
        game.doMove(ROTATE);
        String afterDownOneRotateFour = game.toString();
        String afterDownOneRotateFourExpected = afterDownExpected;

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownOneRotateOneExpected, afterDownOneRotateOne);
        assertEquals(afterDownOneRotateTwoExpected, afterDownOneRotateTwo);
        assertEquals(afterDownOneRotateThreeExpected, afterDownOneRotateThree);
        assertEquals(afterDownOneRotateFourExpected, afterDownOneRotateFour);
    }

    @Test
    public void testRotateZeeQuarce() {
        Game game = new Game(EmojiSet.CLOTHING, new Random(603));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.ZEE, game, NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽👚👚◽◽◽\n" +
                "◽◽◽👚👚◽◽　Next\n" +
                "◽◽◽◽◽◽◽　👘\n" +
                "◽◽◽◽◽◽◽　👘\n" +
                "◽◽◽◽◽◽◽　👘\n" +
                "◽◽◽◽◽◽◽　👘\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　👘\n" +
                "◽◽◽◽◽◽◽　👘\n" +
                "◽◽◽◽◽◽◽　👘\n" +
                "◽◽👚👚◽◽◽　👘\n" +
                "◽◽◽👚👚◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFTORRIGHT_ROTATE_DOWN_PLUMMET, game.listLegalMoves());
        game.doMove(ROTATE);
        String afterDownOneRotateOne = game.toString();
        String afterDownOneRotateOneExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　👘\n" +
                "◽◽◽◽◽◽◽　👘\n" +
                "◽◽◽👚◽◽◽　👘\n" +
                "◽◽👚👚◽◽◽　👘\n" +
                "◽◽👚◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(ROTATE);
        String afterDownOneRotateTwo = game.toString();
        String afterDownOneRotateTwoExpected = afterDownExpected;
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(ROTATE);
        String afterDownOneRotateThree = game.toString();
        String afterDownOneRotateThreeExpected = afterDownOneRotateOneExpected;
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(ROTATE);
        String afterDownOneRotateFour = game.toString();
        String afterDownOneRotateFourExpected = afterDownExpected;

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownOneRotateOneExpected, afterDownOneRotateOne);
        assertEquals(afterDownOneRotateTwoExpected, afterDownOneRotateTwo);
        assertEquals(afterDownOneRotateThreeExpected, afterDownOneRotateThree);
        assertEquals(afterDownOneRotateFourExpected, afterDownOneRotateFour);
    }

    @Test
    public void testRotateTeeQuarce() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(18));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.TEE, game, NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽🐵🐵🐵◽◽\n" +
                "◽◽◽🐵◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐼\n" +
                "◽◽◽◽◽◽◽　🐼🐼\n" +
                "◽◽◽◽◽◽◽　◽🐼\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐼\n" +
                "◽◽◽◽◽◽◽　🐼🐼\n" +
                "◽◽◽◽◽◽◽　◽🐼\n" +
                "◽◽🐵🐵🐵◽◽\n" +
                "◽◽◽🐵◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFTORRIGHT_ROTATE_DOWN_PLUMMET, game.listLegalMoves());
        game.doMove(ROTATE);
        String afterDownOneRotateOne = game.toString();
        String afterDownOneRotateOneExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐼\n" +
                "◽◽◽◽◽◽◽　🐼🐼\n" +
                "◽◽◽🐵◽◽◽　◽🐼\n" +
                "◽◽◽🐵🐵◽◽\n" +
                "◽◽◽🐵◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(ROTATE);
        String afterDownOneRotateTwo = game.toString();
        String afterDownOneRotateTwoExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐼\n" +
                "◽◽◽◽◽◽◽　🐼🐼\n" +
                "◽◽◽🐵◽◽◽　◽🐼\n" +
                "◽◽🐵🐵🐵◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(ROTATE);
        String afterDownOneRotateThree = game.toString();
        String afterDownOneRotateThreeExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐼\n" +
                "◽◽◽◽◽◽◽　🐼🐼\n" +
                "◽◽◽🐵◽◽◽　◽🐼\n" +
                "◽◽🐵🐵◽◽◽\n" +
                "◽◽◽🐵◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(ROTATE);
        String afterDownOneRotateFour = game.toString();
        String afterDownOneRotateFourExpected = afterDownExpected;

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownOneRotateOneExpected, afterDownOneRotateOne);
        assertEquals(afterDownOneRotateTwoExpected, afterDownOneRotateTwo);
        assertEquals(afterDownOneRotateThreeExpected, afterDownOneRotateThree);
        assertEquals(afterDownOneRotateFourExpected, afterDownOneRotateFour);
    }

    @Test
    public void testRotateEllQuarce() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(12));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.ELL, game, NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽🐳🐳🐳◽◽\n" +
                "◽◽🐳◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐌\n" +
                "◽◽◽◽◽◽◽　🐌🐌\n" +
                "◽◽◽◽◽◽◽　◽🐌\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐌\n" +
                "◽◽◽◽◽◽◽　🐌🐌\n" +
                "◽◽◽◽◽◽◽　◽🐌\n" +
                "◽◽🐳🐳🐳◽◽\n" +
                "◽◽🐳◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFTORRIGHT_ROTATE_DOWN_PLUMMET, game.listLegalMoves());
        game.doMove(ROTATE);
        String afterDownOneRotateOne = game.toString();
        String afterDownOneRotateOneExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐌\n" +
                "◽◽◽◽◽◽◽　🐌🐌\n" +
                "◽◽🐳◽◽◽◽　◽🐌\n" +
                "◽◽🐳◽◽◽◽\n" +
                "◽◽🐳🐳◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(ROTATE);
        String afterDownOneRotateTwo = game.toString();
        String afterDownOneRotateTwoExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐌\n" +
                "◽◽◽◽◽◽◽　🐌🐌\n" +
                "◽◽◽◽◽◽◽　◽🐌\n" +
                "◽◽◽◽🐳◽◽\n" +
                "◽◽🐳🐳🐳◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(ROTATE);
        String afterDownOneRotateThree = game.toString();
        String afterDownOneRotateThreeExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐌\n" +
                "◽◽◽◽◽◽◽　🐌🐌\n" +
                "◽◽🐳🐳◽◽◽　◽🐌\n" +
                "◽◽◽🐳◽◽◽\n" +
                "◽◽◽🐳◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(ROTATE);
        String afterDownOneRotateFour = game.toString();
        String afterDownOneRotateFourExpected = afterDownExpected;

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownOneRotateOneExpected, afterDownOneRotateOne);
        assertEquals(afterDownOneRotateTwoExpected, afterDownOneRotateTwo);
        assertEquals(afterDownOneRotateThreeExpected, afterDownOneRotateThree);
        assertEquals(afterDownOneRotateFourExpected, afterDownOneRotateFour);
    }

    @Test
    public void testRotateJayQuarce() {
        Game game = new Game(EmojiSet.FRUIT, new Random(7));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.JAY, game, NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽🍏◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🍏🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🍏🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽🍏◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(ROTATE);
        String afterDownOneRotateOne = game.toString();
        String afterDownOneRotateOneExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🍏🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽🍏🍏◽◽◽　🍏\n" +
                "◽◽🍏◽◽◽◽\n" +
                "◽◽🍏◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(ROTATE);
        String afterDownOneRotateTwo = game.toString();
        String afterDownOneRotateTwoExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🍏🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽🍏◽◽◽◽\n" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(ROTATE);
        String afterDownOneRotateThree = game.toString();
        String afterDownOneRotateThreeExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🍏🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽🍏◽◽◽　🍏\n" +
                "◽◽◽🍏◽◽◽\n" +
                "◽◽🍏🍏◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(ROTATE);
        String afterDownOneRotateFour = game.toString();
        String afterDownOneRotateFourExpected = afterDownExpected;

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownOneRotateOneExpected, afterDownOneRotateOne);
        assertEquals(afterDownOneRotateTwoExpected, afterDownOneRotateTwo);
        assertEquals(afterDownOneRotateThreeExpected, afterDownOneRotateThree);
        assertEquals(afterDownOneRotateFourExpected, afterDownOneRotateFour);
    }

    @Test
    public void testRotateLineQuarce() {
        Game game = new Game(EmojiSet.CLOTHING, new Random(6));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.LINE, game, NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽👘👘👘👘◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽👘👘👘👘◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(DOWN);
        String afterDownTwo = game.toString();
        String afterDownTwoExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽👘👘👘👘◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(ROTATE);
        String afterDownTwoRotateOne = game.toString();
        String afterDownTwoRotateOneExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽　🎽🎽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽👘◽◽◽\n" +
                "◽◽◽👘◽◽◽　Score\n" +
                "◽◽◽👘◽◽◽　0\n" +
                "◽◽◽👘◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        game.doMove(ROTATE);
        String afterDownTwoRotateTwo = game.toString();
        String afterDownTwoRotateTwoExpected = afterDownTwoExpected;
        game.doMove(ROTATE);
        String afterDownTwoRotateThree = game.toString();
        String afterDownTwoRotateThreeExpected = afterDownTwoRotateOneExpected;
        game.doMove(ROTATE);
        String afterDownTwoRotateFour = game.toString();
        String afterDownTwoRotateFourExpected = afterDownTwoExpected;

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownTwoExpected, afterDownTwo);
        assertEquals(afterDownTwoRotateOneExpected, afterDownTwoRotateOne);
        assertEquals(afterDownTwoRotateTwoExpected, afterDownTwoRotateTwo);
        assertEquals(afterDownTwoRotateThreeExpected, afterDownTwoRotateThree);
        assertEquals(afterDownTwoRotateFourExpected, afterDownTwoRotateFour);
    }

    @Test
    public void testGetLegalMovesCantGoLeftOffGrid() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, NORTH, 3, 1, null);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "🐯🐯🐯🐯◽◽◽　🐵🐵\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";

        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(RIGHT_ROTATE_DOWN_PLUMMET, moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testGetLegalMovesCanRotateAtTop() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(3));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.LINE, game, NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽🐯🐯🐯🐯◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐨\n" +
                "◽◽◽◽◽◽◽　🐨\n" +
                "◽◽◽◽◽◽◽　🐨🐨\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        List<Move> moves = game.listLegalMoves();
        assertEquals(expectedStart, start);
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, moves);
        game.doMove(ROTATE);
        String afterRotate = "" +
                "◽◽◽🐯◽◽◽\n" +
                "◽◽◽🐯◽◽◽　Next\n" +
                "◽◽◽🐯◽◽◽　🐨\n" +
                "◽◽◽🐯◽◽◽　🐨\n" +
                "◽◽◽◽◽◽◽　🐨🐨\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, moves);
        assertEquals(afterRotate, game.toString());
        game.doMove(ROTATE);
        String afterTwoRotates = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽🐯🐯🐯🐯◽　🐨\n" +
                "◽◽◽◽◽◽◽　🐨\n" +
                "◽◽◽◽◽◽◽　🐨🐨\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, moves);
        assertEquals(afterTwoRotates, game.toString());
    }

    @Test
    public void testCannotRotateAtTopIfInsufficientRoomBelow() {
        Converter converter = new Converter(new Random(3));
        Game game = converter.makeGameFromJson("{\"thm\":\"FRUIT\",\"thmLen\":57,\"thr\":31," +
                "\"rows\":\"2*.7-..S.4-8*S6.\",\"cur\":{\"t\":\"J\",\"c\":3,\"r\":0,\"d\":\"N\"}," +
                "\"nx\":\"Z\",\"sc\":100,\"id\":1234567890}");
        String start = game.toString();
        String expectedStart = "" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽🍏◽◽　Next\n" +
                "◽◽🍓◽◽◽◽　◽🍎\n" +
                "🍓🍓🍓🍓🍓🍓◽　🍎🍎\n" +
                "🍓🍓🍓🍓🍓🍓◽　🍎\n" +
                "🍓🍓🍓🍓🍓🍓◽\n" +
                "🍓🍓🍓🍓🍓🍓◽\n" +
                "🍓🍓🍓🍓🍓🍓◽　Score\n" +
                "🍓🍓🍓🍓🍓🍓◽　100\n" +
                "🍓🍓🍓🍓🍓🍓◽\n" +
                "🍓🍓🍓🍓🍓🍓◽";
        List<Move> moves = game.listLegalMoves();
        assertEquals(expectedStart, start);
        assertEquals(LEFT_RIGHT_DOWN_PLUMMET, moves);
    }

    @Test
    public void testCannotRotateAtTopIfInsufficientRoomBelowLine() {
        Converter converter = new Converter(new Random(3));
        Game game = converter.makeGameFromJson("{\"thm\":\"FRUIT\",\"thmLen\":57,\"thr\":31," +
                "\"rows\":\"3*.7-8*S6.\",\"cur\":{\"t\":\"I\",\"c\":3,\"r\":0,\"d\":\"N\"}," +
                "\"nx\":\"Z\",\"sc\":100,\"id\":1234567890}");
        String start = game.toString();
        String expectedStart = "" +
                "◽◽🍑🍑🍑🍑◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　◽🍎\n" +
                "🍓🍓🍓🍓🍓🍓◽　🍎🍎\n" +
                "🍓🍓🍓🍓🍓🍓◽　🍎\n" +
                "🍓🍓🍓🍓🍓🍓◽\n" +
                "🍓🍓🍓🍓🍓🍓◽\n" +
                "🍓🍓🍓🍓🍓🍓◽　Score\n" +
                "🍓🍓🍓🍓🍓🍓◽　100\n" +
                "🍓🍓🍓🍓🍓🍓◽\n" +
                "🍓🍓🍓🍓🍓🍓◽";
        assertEquals(expectedStart, start);
        assertEquals(LEFT_RIGHT_DOWN_PLUMMET, game.listLegalMoves());
        game.doMove(DOWN);
        String afterDown = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🍑🍑🍑🍑◽　Next\n" +
                "◽◽◽◽◽◽◽　◽🍎\n" +
                "🍓🍓🍓🍓🍓🍓◽　🍎🍎\n" +
                "🍓🍓🍓🍓🍓🍓◽　🍎\n" +
                "🍓🍓🍓🍓🍓🍓◽\n" +
                "🍓🍓🍓🍓🍓🍓◽\n" +
                "🍓🍓🍓🍓🍓🍓◽　Score\n" +
                "🍓🍓🍓🍓🍓🍓◽　100\n" +
                "🍓🍓🍓🍓🍓🍓◽\n" +
                "🍓🍓🍓🍓🍓🍓◽";
        assertEquals(afterDown, game.toString());
        assertEquals(LEFT_RIGHT_DOWN_PLUMMET, game.listLegalMoves());
        game.doMove(DOWN);
        String afterTwoDown = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽⬛⬛⬛⬛◽　◽🍎\n" +
                "🍓🍓🍓🍓🍓🍓◽　🍎🍎\n" +
                "🍓🍓🍓🍓🍓🍓◽　🍎\n" +
                "🍓🍓🍓🍓🍓🍓◽\n" +
                "🍓🍓🍓🍓🍓🍓◽\n" +
                "🍓🍓🍓🍓🍓🍓◽　Score\n" +
                "🍓🍓🍓🍓🍓🍓◽　100\n" +
                "🍓🍓🍓🍓🍓🍓◽\n" +
                "🍓🍓🍓🍓🍓🍓◽";
        assertEquals(afterTwoDown, game.toString());
        assertEquals(list(LEFT, RIGHT, STOP), game.listLegalMoves());
    }

    @Test
    public void testSquaresCantRotateYo() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(3));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.SQUARE, game, NORTH);
        game.setPiece(shape);
        game.doMove(DOWN);
        String gameString = game.toString();
        String expectedGameString = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐨\n" +
                "◽◽◽◽◽◽◽　🐨\n" +
                "◽◽◽◽◽◽◽　🐨🐨\n" +
                "◽◽◽🐻🐻◽◽\n" +
                "◽◽◽🐻🐻◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";

        List<Move> moves = game.listLegalMoves();
        assertEquals(expectedGameString, gameString);
        assertEquals(LEFT_RIGHT_DOWN_PLUMMET, moves);
    }

    @Test
    public void testGetLegalMovesCanRotateWhenCloseToTop() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(29));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, NORTH, 1, 3, null);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯🐯🐯🐯◽　Next\n" +
                "◽◽◽◽◽◽◽　🐼\n" +
                "◽◽◽◽◽◽◽　🐼🐼\n" +
                "◽◽◽◽◽◽◽　◽🐼\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        List<Move> moves = game.listLegalMoves();
        assertEquals(expectedStart, start);
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, moves);
        game.doMove(ROTATE);
        String afterRotate = "" +
                "◽◽◽🐯◽◽◽\n" +
                "◽◽◽🐯◽◽◽　Next\n" +
                "◽◽◽🐯◽◽◽　🐼\n" +
                "◽◽◽🐯◽◽◽　🐼🐼\n" +
                "◽◽◽◽◽◽◽　◽🐼\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, moves);
        assertEquals(afterRotate, game.toString());
    }

    @Test
    public void testGetLegalMovesCanRotateWhenNotTooCloseToTop() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, NORTH, 2, 3, null);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽🐯🐯🐯🐯◽　🐵\n" +
                "◽◽◽◽◽◽◽　🐵🐵\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testGetLegalMovesCantGoRightOffGrid() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, NORTH, 3, 4, null);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽🐯🐯🐯🐯　🐵🐵\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(LEFT_ROTATE_DOWN_PLUMMET, moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testGetLegalMovesCantRotateOffGrid() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, EAST, 3, 6, null);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽🐯　Next\n" +
                "◽◽◽◽◽◽🐯　🐵\n" +
                "◽◽◽◽◽◽🐯　🐵🐵\n" +
                "◽◽◽◽◽◽🐯　🐵\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(Arrays.asList(LEFT, DOWN, PLUMMET), moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testGetLegalMovesCantRotateOffGridWhileNearButNotTouchingEdge() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, EAST, 3, 5, null);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽🐯◽　Next\n" +
                "◽◽◽◽◽🐯◽　🐵\n" +
                "◽◽◽◽◽🐯◽　🐵🐵\n" +
                "◽◽◽◽◽🐯◽　🐵\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(LEFT_RIGHT_DOWN_PLUMMET, moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testGetLegalMovesCanRotateWhenNotLeavingGrid() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, EAST, 3, 1, null);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽🐯◽◽◽◽◽　Next\n" +
                "◽🐯◽◽◽◽◽　🐵\n" +
                "◽🐯◽◽◽◽◽　🐵🐵\n" +
                "◽🐯◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(expectedStart, start);
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, moves);
        assertEquals("Game state should not have changed", expectedStart, game.toString());
    }

    @Test
    public void testGetLegalMovesCantGoLeftWhenBlockedByTiles() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, EAST, 7, 3, null);
        game.setPiece(shape);
        game.addTile(new Tile(ShapeType.ELL, game), 8, 2);
        game.addTile(new Tile(ShapeType.ELL, game), 9, 2);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 2);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 3);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽　🐵🐵\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽🐯◽◽◽\n" +
                "◽◽◽🐯◽◽◽\n" +
                "◽◽◽🐯◽◽◽　Score\n" +
                "◽◽🐨🐯◽◽◽　0\n" +
                "◽◽🐨◽◽◽◽\n" +
                "◽◽🐨🐨◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(RIGHT_ROTATE_DOWN_PLUMMET, moves);
        assertEquals(expectedStart, start);
    }

    @Test
    public void testGetLegalMovesCantGoRightWhenBlockedByTiles() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, EAST, 7, 2, null);
        game.setPiece(shape);
        game.addTile(new Tile(ShapeType.ELL, game), 8, 3);
        game.addTile(new Tile(ShapeType.ELL, game), 9, 3);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 3);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 4);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽　🐵🐵\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽　Score\n" +
                "◽◽🐯🐨◽◽◽　0\n" +
                "◽◽◽🐨◽◽◽\n" +
                "◽◽◽🐨🐨◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(LEFT_ROTATE_DOWN_PLUMMET, moves);
        assertEquals(expectedStart, start);
    }

    @Test
    public void testGetLegalMovesCantRotateWhenBlockedByTiles() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, EAST, 8, 2, null);
        game.setPiece(shape);
        game.addTile(new Tile(ShapeType.SQUARE, game), 8, 4);
        game.addTile(new Tile(ShapeType.SQUARE, game), 8, 5);
        game.addTile(new Tile(ShapeType.SQUARE, game), 9, 4);
        game.addTile(new Tile(ShapeType.SQUARE, game), 9, 5);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 4);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 5);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽　🐵🐵\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽　Score\n" +
                "◽◽🐯◽🐻🐻◽　0\n" +
                "◽◽🐯◽🐻🐻◽\n" +
                "◽◽◽◽🐨🐨◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(LEFT_RIGHT_DOWN_PLUMMET, moves);
        assertEquals(expectedStart, start);
    }

    @Test
    public void testLeftOrRightComboElected() {
        Game game = new Game(EmojiSet.FRUIT, new Random(7));
        Shape shape = ShapeType.createShapeAtSpawnPoint(ShapeType.JAY, game, NORTH);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽🍏◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🍏🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(DOWN);
        String afterDown = game.toString();
        String afterDownExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🍏🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽🍏◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(LEFTORRIGHT_ROTATE_DOWN_PLUMMET, game.listLegalMoves());
        game.doMove(LEFT_OR_RIGHT);
        String afterLeftOrRightCombo = game.toString();
        assertEquals(list(LEFT, RIGHT), game.listLegalMoves());
        game.doMove(RIGHT);
        String afterRight = game.toString();
        String afterRightExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🍏🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽🍏🍏🍏◽\n" +
                "◽◽◽◽◽🍏◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals("After moving Right, plummet does not need to enabled",
                LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(RIGHT);
        assertEquals("When touching right wall, plummet is enabled because Right is disabled, so " +
                "there is no Left/Right combo option", LEFT_ROTATE_DOWN_PLUMMET, game
                .listLegalMoves());
        game.doMove(DOWN);
        assertEquals("When touching the right wall after down, there should be no 'Left/Right'" +
                " combo option", LEFT_ROTATE_DOWN_PLUMMET, game.listLegalMoves());
        game.doMove(LEFT);
        assertEquals("After Left there should be no Left/Right combo option",
                LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(DOWN);
        assertEquals("After Down when no near walls, there should no Left/Right combo option",
                LEFTORRIGHT_ROTATE_DOWN_PLUMMET, game.listLegalMoves());
        game.doMove(ROTATE);
        assertEquals("After Rotate there should be no Left/Right combo option",
                LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(LEFT);
        game.doMove(LEFT);
        game.doMove(LEFT);
        String afterThreeLefts = game.toString();
        String afterThreeLeftsExpected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🍏🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽　🍏\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "🍏🍏◽◽◽◽◽　Score\n" +
                "🍏◽◽◽◽◽◽　0\n" +
                "🍏◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(afterThreeLeftsExpected, afterThreeLefts);
        assertEquals("After Rotate there should be no Left/Right combo option",
                list(RIGHT, ROTATE, DOWN, PLUMMET), game.listLegalMoves());
        game.doMove(RIGHT);
        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        game.doMove(DOWN);
        assertEquals(LEFT_RIGHT_ROTATE_STOP, game.listLegalMoves());

        assertEquals(expectedStart, start);
        assertEquals(afterDownExpected, afterDown);
        assertEquals(afterDownExpected, afterLeftOrRightCombo);
        assertEquals(afterRightExpected, afterRight);
    }

    @Test
    public void testGetLegalMovesCantGoDownButCanStopWhenTouchingTopOfTiles() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(3));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, EAST, 8, 2, null);
        game.setPiece(shape);
        game.addTile(new Tile(ShapeType.SQUARE, game), 10, 2);
        game.addTile(new Tile(ShapeType.SQUARE, game), 10, 3);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐨\n" +
                "◽◽◽◽◽◽◽　🐨\n" +
                "◽◽◽◽◽◽◽　🐨🐨\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽⬛◽◽◽◽\n" +
                "◽◽⬛◽◽◽◽　Score\n" +
                "◽◽⬛◽◽◽◽　0\n" +
                "◽◽⬛◽◽◽◽\n" +
                "◽◽🐻🐻◽◽◽";
        List<Move> moves = game.listLegalMoves();

        assertEquals(Arrays.asList(LEFT, RIGHT, ROTATE, STOP), moves);
        assertEquals(expectedStart, start);

        game.doMove(STOP);
        String expectedAfterStop = "" +
                "◽◽🐨🐨🐨◽◽\n" +
                "◽◽🐨◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐼\n" +
                "◽◽◽◽◽◽◽　🐼🐼\n" +
                "◽◽◽◽◽◽◽　◽🐼\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽　Score\n" +
                "◽◽🐯◽◽◽◽　0\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐻🐻◽◽◽";
        assertEquals(expectedAfterStop, game.toString());
        game.doMove(DOWN);
        String expectedAfterSpawnAndDown = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽🐨🐨🐨◽◽　🐼\n" +
                "◽◽🐨◽◽◽◽　🐼🐼\n" +
                "◽◽◽◽◽◽◽　◽🐼\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽　Score\n" +
                "◽◽🐯◽◽◽◽　0\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐻🐻◽◽◽";
        assertEquals(expectedAfterSpawnAndDown, game.toString());
    }

    @Test
    public void testStopAtBottomOfGrid() {

        Game game = new Game(EmojiSet.ANIMAL, new Random(61));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, EAST, 8, 2, null);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐨\n" +
                "◽◽◽◽◽◽◽　🐨\n" +
                "◽◽◽◽◽◽◽　🐨🐨\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽　Score\n" +
                "◽◽🐯◽◽◽◽　0\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";

        assertEquals(LEFT_RIGHT_ROTATE_DOWN, game.listLegalMoves());
        assertEquals(expectedStart, start);

        game.doMove(DOWN);
        String afterDown = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐨\n" +
                "◽◽◽◽◽◽◽　🐨\n" +
                "◽◽◽◽◽◽◽　🐨🐨\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽⬛◽◽◽◽　Score\n" +
                "◽◽⬛◽◽◽◽　0\n" +
                "◽◽⬛◽◽◽◽\n" +
                "◽◽⬛◽◽◽◽";
        assertEquals(afterDown, game.toString());

        assertEquals(LEFT_RIGHT_ROTATE_STOP, game.listLegalMoves());
        game.doMove(STOP);
        String afterStop = "" +
                "◽◽🐨🐨🐨◽◽\n" +
                "◽◽🐨◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐼\n" +
                "◽◽◽◽◽◽◽　🐼🐼\n" +
                "◽◽◽◽◽◽◽　◽🐼\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽　Score\n" +
                "◽◽🐯◽◽◽◽　0\n" +
                "◽◽🐯◽◽◽◽\n" +
                "◽◽🐯◽◽◽◽";

        assertEquals(afterStop, game.toString());
    }

    @Test
    public void testDeleteRowsAndClearEntireGrid() {

        Game game = new Game(EmojiSet.AQUATIC, new Random(9));
        Shape shape = ShapeType.createShape(ShapeType.SQUARE, game, EAST, 3, 3, null);
        game.setPiece(shape);

        game.addTile(new Tile(ShapeType.TEE, game), 9, 2);
        game.addTile(new Tile(ShapeType.TEE, game), 9, 5);
        game.addTile(new Tile(ShapeType.TEE, game), 9, 6);
        game.addTile(new Tile(ShapeType.TEE, game), 10, 5);
        game.addTile(new Tile(ShapeType.ZEE, game), 9, 0);
        game.addTile(new Tile(ShapeType.ZEE, game), 9, 1);
        game.addTile(new Tile(ShapeType.ZEE, game), 10, 1);
        game.addTile(new Tile(ShapeType.ZEE, game), 10, 2);
        game.addTile(new Tile(ShapeType.ZEE, game), 10, 2);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 0);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 6);

        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　◽🐙\n" +
                "◽◽◽🐟🐟◽◽　🐙🐙\n" +
                "◽◽◽🐟🐟◽◽　🐙\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "🐙🐙🐠◽◽🐠🐠\n" +
                "🐳🐙🐙◽◽🐠🐳";

        assertEquals(Arrays.asList(LEFT, RIGHT, DOWN, PLUMMET), game.listLegalMoves());
        assertEquals(expectedStart, game.toString());

        game.doMove(PLUMMET);
        String afterDown = "" +
                "◽◽🐙🐙◽◽◽\n" +
                "◽◽◽🐙🐙◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐡\n" +
                "◽◽◽◽◽◽◽　🐡\n" +
                "◽◽◽◽◽◽◽　🐡\n" +
                "◽◽◽◽◽◽◽　🐡\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　1250\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        assertEquals(afterDown, game.toString());
    }

    @Test
    public void testDeleteRows() {

        Game game = new Game(EmojiSet.AQUATIC, new Random(9));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, EAST, 4, 4, null);
        game.setPiece(shape);
        game.addTile(new Tile(ShapeType.LINE, game), 4, 3);
        game.addTile(new Tile(ShapeType.LINE, game), 5, 3);
        game.addTile(new Tile(ShapeType.LINE, game), 6, 3);
        game.addTile(new Tile(ShapeType.LINE, game), 7, 3);
        game.addTile(new Tile(ShapeType.JAY, game), 6, 1);
        game.addTile(new Tile(ShapeType.JAY, game), 6, 2);
        game.addTile(new Tile(ShapeType.JAY, game), 7, 2);
        game.addTile(new Tile(ShapeType.JAY, game), 8, 2);
        game.addTile(new Tile(ShapeType.SQUARE, game), 6, 5);
        game.addTile(new Tile(ShapeType.SQUARE, game), 6, 6);
        game.addTile(new Tile(ShapeType.SQUARE, game), 7, 5);
        game.addTile(new Tile(ShapeType.SQUARE, game), 7, 6);
        game.addTile(new Tile(ShapeType.SQUARE, game), 7, 0);
        game.addTile(new Tile(ShapeType.SQUARE, game), 7, 1);
        game.addTile(new Tile(ShapeType.SQUARE, game), 8, 0);
        game.addTile(new Tile(ShapeType.SQUARE, game), 8, 1);
        game.addTile(new Tile(ShapeType.TEE, game), 8, 3);
        game.addTile(new Tile(ShapeType.TEE, game), 9, 2);
        game.addTile(new Tile(ShapeType.TEE, game), 9, 3);
        game.addTile(new Tile(ShapeType.TEE, game), 10, 3);
        game.addTile(new Tile(ShapeType.TEE, game), 8, 5);
        game.addTile(new Tile(ShapeType.TEE, game), 9, 5);
        game.addTile(new Tile(ShapeType.TEE, game), 9, 6);
        game.addTile(new Tile(ShapeType.TEE, game), 10, 5);
        game.addTile(new Tile(ShapeType.ZEE, game), 9, 0);
        game.addTile(new Tile(ShapeType.ZEE, game), 9, 1);
        game.addTile(new Tile(ShapeType.ZEE, game), 10, 1);
        game.addTile(new Tile(ShapeType.ZEE, game), 10, 2);
        game.addTile(new Tile(ShapeType.ZEE, game), 10, 2);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 0);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 6);

        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽🐡◽◽　◽🐙\n" +
                "◽◽◽◽🐡◽◽　🐙🐙\n" +
                "◽◽◽🐡🐡◽◽　🐙\n" +
                "◽◽◽🐡🐡◽◽\n" +
                "◽🐬🐬🐡◽🐟🐟\n" +
                "🐟🐟🐬🐡◽🐟🐟　Score\n" +
                "🐟🐟🐬🐠◽🐠◽　0\n" +
                "🐙🐙🐠🐠◽🐠🐠\n" +
                "🐳🐙🐙🐠◽🐠🐳";

        assertEquals(Arrays.asList(RIGHT, DOWN, PLUMMET), game.listLegalMoves());
        assertEquals(expectedStart, game.toString());

        game.doMove(DOWN);
        String afterDown = "" +
                "◽◽🐙🐙◽◽◽\n" +
                "◽◽◽🐙🐙◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐡\n" +
                "◽◽◽◽◽◽◽　🐡\n" +
                "◽◽◽◽◽◽◽　🐡\n" +
                "◽◽◽◽◽◽◽　🐡\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🐡◽◽◽　Score\n" +
                "◽◽◽🐡◽◽◽　525\n" +
                "◽🐬🐬🐡◽🐟🐟\n" +
                "🐟🐟🐬🐠🐡🐠◽";
        assertEquals(afterDown, game.toString());
    }

    @Test
    public void testDeleteNegativeOneRows() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(2));
        try {
            game.scoreDeletedRows(-1, GridOccupancy.OCCUPIED);
            fail("Expected RuntimeException");
        } catch (RuntimeException e) {
            assertEquals("Can't delete -1 rows", e.getMessage());
        }
    }

    @Test
    public void testDeleteZeroRows() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(2)).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(0, GridOccupancy.OCCUPIED);
        assertEquals(400, game.getScore());
    }

    @Test
    public void testDeleteZeroRowsWithEmptyGrid() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(2)).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(0, GridOccupancy.VACANT);
        assertEquals(400, game.getScore());
    }

    @Test
    public void testDeleteOneRow() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(6)).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(1, GridOccupancy.OCCUPIED);
        assertEquals(500, game.getScore());
    }

    @Test
    public void testDeleteOneRowAndClearGameGrid() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(6)).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(1, GridOccupancy.VACANT);
        assertEquals(1500, game.getScore());
    }

    @Test
    public void testDeleteTwoRows() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(7)).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(2, GridOccupancy.OCCUPIED);
        assertEquals(650, game.getScore());
    }

    @Test
    public void testDeleteTwoRowsAndClearGameGrid() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(7)).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(2, GridOccupancy.VACANT);
        assertEquals(1650, game.getScore());
    }

    @Test
    public void testDeleteThreeRows() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(8)).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(3, GridOccupancy.OCCUPIED);
        assertEquals(925, game.getScore());
    }

    @Test
    public void testDeleteThreeRowsAndClearGameGrid() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(8)).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(3, GridOccupancy.VACANT);
        assertEquals(1925, game.getScore());
    }

    @Test
    public void testDeleteFourRows() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(9));
        game.setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(4, GridOccupancy.OCCUPIED);
        assertEquals(1400, game.getScore());
    }

    @Test
    public void testDeleteFourRowsAndClearGameGrid() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(9));
        game.setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(4, GridOccupancy.VACANT);
        assertEquals(2400, game.getScore());
    }

    @Test
    public void testDeleteFiveRows() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(5)).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(5, GridOccupancy.OCCUPIED);
        assertEquals(1650, game.getScore());
    }

    @Test
    public void testDeleteFiveRowsAndClearGameGrid() {
        Game game = new Game(EmojiSet.AQUATIC, new Random(5)).setScore(400);
        assertEquals(400, game.getScore());
        game.scoreDeletedRows(5, GridOccupancy.VACANT);
        assertEquals(2650, game.getScore());
    }

    @Test
    public void testCountBlankRowsBelowPieceWithTiles() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, SOUTH, 2, 2, null);
        game.setPiece(shape);
        game.addTile(new Tile(ShapeType.ELL, game), 8, 3);
        game.addTile(new Tile(ShapeType.ELL, game), 9, 3);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 3);
        game.addTile(new Tile(ShapeType.ELL, game), 10, 4);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽🐯🐯🐯🐯◽◽　🐵\n" +
                "◽◽◽◽◽◽◽　🐵🐵\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽🐨◽◽◽　0\n" +
                "◽◽◽🐨◽◽◽\n" +
                "◽◽◽🐨🐨◽◽";
        int blankRowCount = game.countBlankRowsBelowPiece();

        assertEquals(5, blankRowCount);
        assertEquals(expectedStart, start);
    }

    @Test
    public void testCountBlankRowsBelowPieceNoTiles() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, SOUTH, 2, 2, null);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽🐯🐯🐯🐯◽◽　🐵\n" +
                "◽◽◽◽◽◽◽　🐵🐵\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽";
        int blankRowCount = game.countBlankRowsBelowPiece();

        assertEquals(8, blankRowCount);
        assertEquals(expectedStart, start);
    }

    @Test
    public void testCountBlankRowsBelowBottomRow() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        Shape shape = ShapeType.createShape(ShapeType.LINE, game, SOUTH, 10, 2, null);
        game.setPiece(shape);
        String start = game.toString();
        String expectedStart = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Next\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽　🐵🐵\n" +
                "◽◽◽◽◽◽◽　🐵\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　0\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽⬛⬛⬛⬛◽◽";
        int blankRowCount = game.countBlankRowsBelowPiece();

        assertEquals(0, blankRowCount);
        assertEquals(expectedStart, start);
    }

    @Test
    public void testHalfRoundedUpMinimumOne() {
        Game game = new Game(EmojiSet.ANIMAL, new Random(53));
        assertEquals(1, game.halfRoundedUpMinimumOne(0));
        assertEquals(1, game.halfRoundedUpMinimumOne(1));
        assertEquals(1, game.halfRoundedUpMinimumOne(2));
        assertEquals(2, game.halfRoundedUpMinimumOne(3));
        assertEquals(2, game.halfRoundedUpMinimumOne(4));
        assertEquals(3, game.halfRoundedUpMinimumOne(5));
        assertEquals(3, game.halfRoundedUpMinimumOne(6));
        assertEquals(4, game.halfRoundedUpMinimumOne(7));
        assertEquals(4, game.halfRoundedUpMinimumOne(8));
        assertEquals(5, game.halfRoundedUpMinimumOne(9));
        assertEquals(5, game.halfRoundedUpMinimumOne(10));
        assertEquals(6, game.halfRoundedUpMinimumOne(11));
        assertEquals(6, game.halfRoundedUpMinimumOne(12));
    }
}
