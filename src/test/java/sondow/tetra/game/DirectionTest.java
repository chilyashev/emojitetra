package sondow.tetra.game;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DirectionTest {

    @Test
    public void testFromInitial() {
        assertEquals(Direction.NORTH, Direction.fromInitial("N"));
        assertEquals(Direction.EAST, Direction.fromInitial("E"));
        assertEquals(Direction.SOUTH, Direction.fromInitial("S"));
        assertEquals(Direction.WEST, Direction.fromInitial("W"));
        Throwable t = null;
        try {
            Direction.fromInitial("P");
        } catch (NoSuchDirectionException e) {
            t = e;
            assertEquals("No Direction for P", t.getMessage());
        }
        assertNotNull(t);
    }

}
