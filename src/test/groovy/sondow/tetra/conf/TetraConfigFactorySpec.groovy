package sondow.tetra.conf

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import spock.lang.Specification
import twitter4j.conf.Configuration

class TetraConfigFactorySpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    def "configure should populate a configuration with environment variables"() {
        setup:
        String filler = Environment.SPACE_FILLER
        envVars.set("CRED_AWS_ACCESS_KEY", "123")
        envVars.set("CRED_AWS_SECRET_KEY", "456")
        envVars.set("twitter_account", "cartoons")
        envVars.set("cartoons_twitter4j_oauth_accessToken", "${filler}fredflintstone")
        envVars.set("cartoons_twitter4j_oauth_accessTokenSecret", "${filler}bugsbunny")

        when:
        Configuration config = new TetraConfigFactory().configure().getTwitterConfig()

        then:
        with(config) {
            OAuthAccessToken == 'fredflintstone'
            OAuthAccessTokenSecret == 'bugsbunny'
        }
    }

    def "configure should populate a configuration with account specific env vars"() {
        setup:
        String filler = Environment.SPACE_FILLER
        envVars.set("CRED_AWS_ACCESS_KEY", "123")
        envVars.set("CRED_AWS_SECRET_KEY", "456")
        envVars.set("twitter_account", "picardtips")
        envVars.set("picardtips_twitter4j_oauth_accessToken", "${filler}therearefourlight")
        envVars.set("picardtips_twitter4j_oauth_accessTokenSecret", "${filler}engage")
        envVars.set("rikergoogling_twitter4j_oauth_accessToken", "${filler}jazz")
        envVars.set("rikergoogling_twitter4j_oauth_accessTokenSecret", "${filler}beard")

        when:
        Configuration config = new TetraConfigFactory().configure().getTwitterConfig()

        then:
        with(config) {
            OAuthAccessToken == 'therearefourlight'
            OAuthAccessTokenSecret == 'engage'
        }
    }
}
