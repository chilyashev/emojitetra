package sondow.tetra.game

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import sondow.tetra.conf.FileClerk
import sondow.tetra.conf.Time
import sondow.tetra.io.Database
import sondow.tetra.io.Outcome
import sondow.tetra.io.TwitterPollMaker
import spock.lang.Specification
import twitter4j.Card
import twitter4j.Choice
import twitter4j.Status
import twitter4j.StatusWithCard

import java.time.ZonedDateTime

class PlayerSpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    private void initEnvironment() {
        envVars.set('twitter_account', 'EmojiTetraGamma')
        envVars.set('EmojiTetraGamma_twitter4j_oauth_accessToken', 'ghostpowder')
        envVars.set('EmojiTetraGamma_twitter4j_oauth_accessTokenSecret', 'auntfrenchie')
        envVars.set('CRED_AWS_ACCESS_KEY', 'lemongiant')
        envVars.set('CRED_AWS_SECRET_KEY', 'truthfulmarionette')
    }

    private String initialTeeGame = '' +
            '◽◽😱😱😱◽◽\n' +
            '◽◽◽😱◽◽◽　Next\n' +
            '◽◽◽◽◽◽◽　🎃\n' +
            '◽◽◽◽◽◽◽　🎃\n' +
            '◽◽◽◽◽◽◽　🎃\n' +
            '◽◽◽◽◽◽◽　🎃\n' +
            '◽◽◽◽◽◽◽\n' +
            '◽◽◽◽◽◽◽　Score\n' +
            '◽◽◽◽◽◽◽　0\n' +
            '◽◽◽◽◽◽◽\n' +
            '◽◽◽◽◽◽◽'

    private List<String> startingMoves = ['⬅️ Left', '➡️ Right', '🔄 Rotate', '⬇️ Down',]

    private List<String> afterDownMoves = ['↔️ Left or Right', '🔄 Rotate', '⬇️ Down', '⏬ Plummet']

    def "should tweet without a poll when game ends"() {
        setup:
        initEnvironment()
        Database database = Mock(Database)
        TwitterPollMaker pollMaker = Mock(TwitterPollMaker)
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard previousTweet = Mock(StatusWithCard)
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("⬅️ Left", 0),
                new Choice("➡️️ Right", 0),
                new Choice("⬇️ Stop", 30) // Super majority
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false

        Status tweet = Mock(Status)

        when:
        Outcome outcome = player.play()

        then:
        1 * database.readGameState() >> '{"thm": "FRUIT","thmLen": 57,"thr": 23,"rows": ' +
                '".7-LL.5-.TOOJ3-T3.ZZJ-L.T3OO-LLSJ.SS-.SSJSSL-SSOO.LL-.TOOJ3-T3.ZZJ-.I4ZZ",' +
                '"cur": {"t": "J","c": 3,"r": 0,"d": "S"},"nx": "L","sc": 1500,"id": 47563}'
        1 * pollMaker.readPreviousTweet(47563L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        1 * previousTweet.getId() >> 47563L
        1 * pollMaker.tweet('' +
                '◽◽🍏◽◽◽◽\n' +
                '🍉🍉🍏🍏🍏◽◽　Next\n' +
                '◽🍋🍇🍇🍏🍏🍏　🍉\n' +
                '☁️☁️☁️☁️☁️☁️☁️　🍉\n' +
                '☁️🇬 🇦 🇲 🇪☁️☁️　🍉🍉\n' +
                '☁️☁️🇴 🇻 🇪 🇷☁️\n' +
                '☁️☁️☁️☁️☁️☁️☁️\n' +
                '🍓🍓🍇🍇◽🍉🍉　Score\n' +
                '◽🍋🍇🍇🍏🍏🍏　1500\n' +
                '🍋🍋🍋◽🍎🍎🍏\n' +
                '◽🍑🍑🍑🍑🍎🍎', 47563L, null) >> tweet
        1 * tweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T06:34:55Z')
        1 * database.deleteGameState()
        outcome.tweet == tweet
        0 * _._
    }

    def "should start new twitter thread when database starts empty"() {
        setup:
        initEnvironment()
        Database database = Mock(Database)
        TwitterPollMaker pollMaker = Mock(TwitterPollMaker)
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        Status tweet = Mock(Status)

        when:
        Outcome outcome = player.play()

        then:
        1 * database.readGameState() >> null
        1 * pollMaker.readPreviousTweet(null) >> null
        1 * pollMaker.postPoll(20, initialTeeGame, startingMoves, null) >> tweet
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T15:02:55Z')
        1 * time.waitASec()
        1 * pollMaker.tweet('Game tip\n\nThe all-time high score is in my account bio.', null, null)
        1 * database.writeGameState('{"sc":0,"cur":{"r":0,"c":3,"t":"T","d":"N"},' +
                '"thm":"HALLOWEEN","nx":"I","id":1234,"rows":"9*.7-2*.7","thr":1}')
        1 * tweet.getId() >> 1234L
        0 * _._
        outcome.tweet == tweet
    }

    def "should continue thread when database has game state, with super majority"() {
        setup:
        initEnvironment()
        Database database = Mock(Database)
        TwitterPollMaker pollMaker = Mock(TwitterPollMaker)
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard previousTweet = Mock(StatusWithCard)
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("⬅️ Left", 0),
                new Choice("➡️️ Right", 0),
                new Choice("🔄 Rotate", 0),
                new Choice("⬇️ Down", 30) // Super majority
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false
        String gameText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽◽◽◽◽　◽🍎\n' +
                '◽◽◽◽◽◽◽　🍎🍎\n' +
                '◽◽◽🍏◽◽◽　🍎\n' +
                '◽◽◽🍏◽◽◽\n' +
                '◽◽🍏🍏◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽◽◽◽◽　100\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        Status tweet = Mock(Status)

        when:
        Outcome outcome = player.play()

        then:
        1 * database.readGameState() >> '{"thm": "FRUIT","thmLen": 57,"thr": 31,' +
                '"rows": "9*.7-.7-.I4ZZ","cur": {"t": "J","c": 3,"r": 2,"d": "E","p": "dn"},' +
                '"nx": "Z","sc": 100,"id": 1234567890}'
        1 * pollMaker.readPreviousTweet(1234567890L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        1 * previousTweet.getId() >> 1234567890L
        1 * pollMaker.postPoll(20, gameText, afterDownMoves, 1234567890L) >> tweet
        1 * tweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T06:34:55Z')
        1 * database.writeGameState('{"sc":100,"cur":{"p":"dn","r":5,"c":3,"t":"J","d":"E"},' +
                '"thm":"FRUIT","nx":"Z","id":87654,"rows":"9*.7-.7-.I4ZZ","thr":32}')
        outcome.tweet == tweet
        0 * _._
    }

    def 'should start new thread after thread gets long'() {
        setup:
        initEnvironment()
        Database database = Mock(Database)
        TwitterPollMaker pollMaker = Mock(TwitterPollMaker)
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard oldGameTweet = Mock(StatusWithCard)
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("⬅️ Left", 30), // Super majority
                new Choice("➡️️ Right", 0),
                new Choice("🔄 Rotate", 0),
                new Choice("⬇️ Down", 0)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false

        Status newGameTweet = Mock(Status)
        String gameText = '' +
                '◽🍏◽◽◽◽◽\n' +
                '◽🍏🍏🍏◽◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🍉\n' +
                '◽◽◽🍋◽◽◽　🍉\n' +
                '🍉◽🍋🍋🍋🍇🍇　🍉🍉\n' +
                '🍉🍉🍓🍏◽🍓🍓\n' +
                '◽🍓🍓🍏🍓🍓🍉\n' +
                '🍓🍓🍇🍇◽🍉🍉　Score\n' +
                '◽🍋🍇🍇🍏🍏🍏　1500\n' +
                '🍋🍋🍋◽🍎🍎🍏\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        Status introTweet = Mock(Status)
        Status outroTweet = Mock(Status)
        File file = new FileClerk().getFile("emojitetrarotations.gif")

        when:
        player.play()

        then:
        1 * database.readGameState() >> '{"thm":"FRUIT","thmLen":57,"thr":51,' +
                '"rows":"3*.7-.3T.3-L.T3OO-LLSJ.SS-.SSJSSL-SSOO.LL-.TOOJ3-T3.ZZJ-.I4ZZ",' +
                '"cur":{"t":"J","c":3,"r":0,"d":"S"},"nx":"L","sc":1500,"id":987654321}'
        1 * pollMaker.readPreviousTweet(987654321L) >> oldGameTweet
        2 * oldGameTweet.getCard() >> previousPoll
        2 * oldGameTweet.getId() >> 987654321L
        1 * pollMaker.tweet('Continuing game from thread… https://twitter' +
                '.com/EmojiTetraGamma/status/987654321', null, null) >> introTweet
        1 * introTweet.getId() >> 898989898L
        1 * pollMaker.postPoll(20, gameText, startingMoves, 898989898L) >> newGameTweet
        1 * pollMaker.tweet('Game continues in new thread… https://twitter' +
                '.com/EmojiTetraGamma/status/87654', 987654321L, null) >> outroTweet
        2 * newGameTweet.getId() >> 87654L
        1 * time.waitASec()
        1 * pollMaker.tweet("Here's a gif showing the rotation rules for the shapes that rotate" +
                ".", null, file)
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-21T17:04:55Z')
        1 * database.writeGameState('{"sc":1500,"cur":{"p":"lf","r":0,"c":2,"t":"J","d":"S"},' +
                '"thm":"FRUIT","nx":"L","id":87654,"rows":"3*.7-.3T.3-L.T3OO-LLSJ.SS-.SSJSSL-SSOO' +
                '.LL-.TOOJ3-T3.ZZJ-.I4ZZ","thr":1}')
        0 * _._
    }

}
